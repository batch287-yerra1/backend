let num = 2;
let getCube = Math.pow(num,3);

console.log(`The cube of ${num} is ${getCube}.`);

let address = ['258', 'Washington', 'Ave NW', 'California', '90011' ];
let [streetNo, city, state, country, pinCode ] = address;
console.log(`I live at ${streetNo} ${city} ${state}, ${country} ${pinCode}`);

let animal = {
	name: 'Lolong',
	family: 'crocodile',
	livesIn: 'saltwater',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
};

let {name, family, livesIn, weight, measurement} = animal;
console.log(`${name} was a ${livesIn} ${family}. He weighed at ${weight} with a measurement of ${measurement}.`);

let numbers = [1, 5, 10, 17, 21];

numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((a,b) => a+b);
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let frankie = new Dog ('Frankie', 5, "Miniature Dachshund");
console.log(frankie);