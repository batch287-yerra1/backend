console.log("Hellow World!!");

// Functions
	// Functions in JS are lines/blocks of codes that our device/application/browser to perform a certain task/statement when called/invokes.
	
	// Function Declaration
	// Defines a functio with specified parameters

	// Syntax:
	/*
		function functionName(){
			code block (-statement-)
		};
	*/

		function printName(){
			console.log("My name is Rohith");
		};


		/*
			function keyword - used to defined a javascript functions functionName - the function name. Function are named to be able to use later in the code.
			function ({}) - the statement which comprises the body of the function. This is where the code to be executed.
		*/

		printName();

// Function Invocation 
	// The code block and statements inside a function is not immediately executed when then function is defined.

		printName();
		// Invoking/calling the functions that we declared.

// function Declaration vs Function Expressions

	// Function Declaration
		//  A function can be created through function declaration by using keyword and adding a function name.

		declaraedFuncton();
		// declared functions can be hoisted. As long as the function has been defined.

		function declaraedFuncton(){
			console.log("Hellow world, from declaredFunction()");
		};

		declaraedFuncton();

	// Function Expressions
		// A function can also be stored in a variable. This is what we called function expression.

		// variableFunction();  //Function Expression cannot be hoisted.

		let variableFunction = function() {
			console.log("H3ll0w again!!");
		};

		variableFunction();
		console.log(variableFunction); // This one will work and will just only show the actual function inside variableFunction

		let functExpression =function funcName(){
			console.log('Hellow from the other side!');
		};

		functExpression();
		// functExpression(funcName);

	// Re-assignment declared functions

		declaraedFuncton = function(){
			console.log('This is the updated declaraedFuncton');
		};

		declaraedFuncton();

		functExpression = function(){
			console.log('updated functExpression');
		};

		functExpression();

	// Re-assign from keywords const

		const constantFunc = function(){
			console.log("Initialised with const!");
		};

		constantFunc();

		// constantFunc = function(){
		// 	console.log("This is the new constantFunc");
		// }; //result will be an error

// Function scoping

	//Scope is the accessibility (visibility/jurisdiction/availability) of variables within our program

		/*
			JS Variables has 3 types of scope:
				1.local/block scope
				2.golbal scope
				3.function scope
		*/

	{
		let localVar = "Armando Perez";
		console.log(localVar);
	}

	let globalVar = "Mr. worldwide";

	console.log(globalVar);

	// Function Scope

	 	// JS has functon: Each function creates a new scope

		function showNames(){
			var functionVar = "jJoe";
			let functionLet = "Jane";
			const functionConst = "Jhon";

			console.log(functionVar);
			console.log(functionLet);
			console.log(functionConst);
		};

		showNames();

		var functionVar = "Mike";
		let functionLet = "Scott";
		const functionConst = "Den";

		console.log(functionVar);
		console.log(functionLet);
		console.log(functionConst);

	// Nested Functions

		// You can create another function inside a function.

		function mynewFunction(){
			let name = "Jane";
			function nestedFunction(){
				let nestedName = "Jhon";
				console.log(name);
				console.log(nestedName);
			};

			nestedFunction();
		};

		mynewFunction();


	// Function and Global Scoped Variables

		// Global Scoped Variable
		let globalName = "Alexandro";

		function mynewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
		};

		mynewFunction2();

// Using Alert()
	// alert() allows us toshow a small window at the top of our browser page to shoe information to our users.

		// alert() Synrtax:
		// alert("<Message In String>");

		alert("Hellow Wolrld?");

		function showSampleAlert(){
			alert("Hellow, User! Hope you have great day!");
		};

		showSampleAlert();

		console.log("I will only be log in the console when the alert is dismissed!");

		// Notes on the use of alert();
			// Show only an alert() for short dialoges/messages to the user.
			// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// Using prompt()
	// prompt() allows us to show a small window at the top of the browser to gather user input.

	let samplePromt = prompt("Enter your name.");

	console.log("Hello, " + samplePromt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your First Name");
		let lastName = prompt("Enter your Last Name");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
		alert("Hello, " + firstName + " " + lastName + "!");
		alert("Welcome to my page!");
	};

	printWelcomeMessage();

// Function Naming Conventions
	// Function names should be definitive of the task it will perform.It is usually contains a verb.

	function getCourse(){
		let courses = ['Science 101', 'Math 101', 'English 101'];
		console.log(courses);
	};

	getCourse();

	// Avoid generic names to avoid confusion within your code.


	function get(){
		let name = "Jaime";
		console.log(name);
	};

	get();

	// Avoid pointless and inaapropriate function names.

	function foo(){
		console.log('Nothing!');
	};

	foo();

	// Name your functions in small caps. Follow camelCase when naming variable and function.

	function displayCarInfo(){
		console.log('Brand: Toyota');
		console.log('Type: Sedan');
		console.log('Price: 1,500,000 Php');
	};

	displayCarInfo();


