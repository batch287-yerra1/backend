/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function objec1(){
		let fullName = prompt("Enter your Full Name: ");
		let age = prompt("Enter your Age: ");
		let location = prompt("Enter your Location: ");

		console.log("Hello! " + fullName);
		console.log("Your are " + age + " years old.");
		console.log("You live in " + location);
	}

	objec1();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function objec2(){
		console.log("My favourite Musical Bands and Artists:");
		console.log("1. The Local Train");
		console.log("2. Agnee");
		console.log("3. Chowrastaa");
		console.log("4. Billie Ellish");
		console.log("5. A R Rahman");
	}

	objec2();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function objec3(){
		console.log("All Time Favourite Movies");
		console.log("Baahubali: The Beginning");
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("Baahubali 2: The Conclusion");
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("Evil");
		console.log("Rotten Tomatoes Rating: 95%");
		console.log("Jhon Wick: Chapter 4");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("Reality");
		console.log("Rotten Tomatoes Rating: 92%");

	}

	objec3();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


    let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
    };

    printFriends();
	// console.log(friend1);
	// console.log(friend2);
