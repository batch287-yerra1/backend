const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0){

			return true

		// No duplicate email found
		// The user is not yet registed in the database
		} else {
			return false
		}
	})
}


module.exports.registerUser = (reqBody)=> {

	console.log(reqBody);

	let newUser = new User({
		firstName:reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({email: reqBody.email}).then(result => {

		console.log(result);

		// Email dosen't exist
		if(result == null){
			return false;
		} else {
			// We now know that the email is correct, is password correct also?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Correctpassword
			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			// Password incorrect
			} else {
				return false
			};
		};
	});
};

module.exports.detailsUser = (reqBody)=> {

	console.log(reqBody); // Will return the key/value of pair of userId: reqbody.Id

	// Changes the value of the user's password to an wmpty string when returned to the frontend
	// Not doing so will expose the user's password which will also not be needed in other parts of our application
	// Unlike in the "regiser" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only th einformation that we will be sending back to the frontend application
	return User.findById(reqBody.userId).then(result => {
		if(result == null){
			return ` we can't find details with this id`
		} else{
			result.password = "";
			console.log(result);
			return result;
		}
	})

	// return User.findById(reqBody.id).then(result => {
	// 	result.password = "";
	// 	return result;
	// })
	
};

module.exports.enroll = async (data) => {

	//1st Await Update first the User Models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	};
};