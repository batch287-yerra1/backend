console.log("Hellow World?");

// [ SECTION ] Functions

	// [ SUB-SECTION ] Parameters and Arguements

	// Functions in JS are lines/blocks of codes that tell our device/applcation/browser to perform a certain task when called/invoked/triggered.

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, " + nickname);
	};

	//printInput();

	// For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

	// name11 ONLY ACTS LIKE A VARIABLE

	function printName(name11){
		//name11 is the parameter
		console.log("Hi, " + name11);
	};

	// console.log(name11); // this one is error as it is not defined.

	printName("Juana"); //"Juana" is arguement.

	// You can directly pass data into the function. The function can then call/use that data which is refferred as "name11" within the function.

	printName("Carlo");

	// When the "printName()" function is called again, it stores the value of "Carlo" in the parameter "name11" then uses it to print a message.

	// Variables can also be passed as an arguement.
	let sampleVariable = "Yui";

	printName(sampleVariable);

	// Function arguements cannot be used by a function if there are no parameters provided within the function.

	printName(); //It will be Hi!, undefiened

	function checkDivisibilityBY8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + "divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBY8(12);
	checkDivisibilityBY8(52);

	// You can also do the smae using prompt(), hoever, take note the prompt() outputs a string. Strings are not ideal for mathematical computaions

// Functions as Arguements

	// Function parameters can also accept other function as arguements

	function arguementFunction(){
		console.log("This function was passed as an arguement before the message was printed.")
	};

	// function invokedFunction(arguementFunction){
	// 	arguementFunction();
	// };

	function invokedFunction(iAmNotRelated){
		iAmNotRelated();
		arguementFunction();
	};

	invokedFunction(arguementFunction);

	// Adding and removing the parenthese "()" impacts the output of JS heavily.

	console.log(arguementFunction);
	// Will provide information about a function inthe console using console.log();

// Using multiple parameters

	// Multiple "arguements" will correspond to the number of "parameters" declasred in a function in succeding order.

	function creteFullName(firstName, middleNmae, lastName){
		console.log(firstName + " " + middleNmae + " " + lastName);

		console.log( middleNmae + " " + lastName + " " + firstName);
	};

	creteFullName("Rohith", "Kumar", "Yerra");

	creteFullName("Rohith", "Yerra");

	creteFullName("Rohith", "Kumar", "Yerra", "hey");

	// In JS, providing more/less arguements than the expected parameters will not return an error.

	// Using Variable as Arguements

	let firstName = "Dwayne";
	let secondName = "the Rock";
	let lastName = "Johnson";

	creteFullName(firstName, secondName, lastName);

// [ SECTION ] The Return statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

	function returnFullName(firstName, middleNmae, lastName){
		console.log("This message is from console.log1");
		return firstName + " " + middleNmae + " " + lastName;
		console.log("This message is from console.log2");
	};

	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	// In our example, the "returnFullName" function was invoked called inthe same line as declaring a variable.

	// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statmentis ignored because it ends the function exectuion.

	// In this example, console.log() will print the returned value of the returnFullNmae() function.
	console.log(returnFullName(firstName, secondName, lastName));

	// You can also create a variable inside the function of caontain the result and return that variable instead.

	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	};

	let myAddress = returnAddress("Srikakulam city", "India");
	console.log(myAddress);

	// On the other hand, when a function the only has console.log() to display its result will be undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	};

	let user1 = printPlayerInfo("knight_white", 95, "paladin");
	console.log(user1);

	// Returns undefined because printPlayerInfo return nothing. It only console.log the details.

	// You cannot save any value from printPlayerInfo() because it does not return anything.

	
