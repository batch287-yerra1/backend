// 

console.log(fetch('https://jsonplaceholder.typicode.com/todos'));
fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json) => console.log(json.map(x => x.title)));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json) => {
		console.log(json);
		console.log(`The items ${title} on the list hasa status of ${completed}`)
	});

	fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Created To Do List Item",
			completed: false,
			userId: 1,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));



	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Updated To Do List Item",
			description: "To update the my to do list with a different data structure",
			dateCompleted: "Pending",
			status: "Pending",
			userId: 1,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'DELETE'
	});