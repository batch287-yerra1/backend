const express = require("express");
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended:false}));

app.get("/home", (request, response) => {
	response.send('Welcome to the home page!!');
});

let items = [
		{
			"username": "Brandon",
			"password": "brandon@mail.com"
		},
		{
			"username": "Jobert",
			"password": "jobert@mail.com"
		}
	]


app.get("/items", (request, response) => {
	response.send(items);
});


app.delete("/delete-item", (req, res) => {

	let message;

	console.log(items.length);

	for(let i = 0; i < items.length; i++){

		if(req.body.username == items[i].username){

 			items.splice(i,1);

			message = `User ${req.body.username} has been deleted.`;

			console.log("Newly updated mock database:");
			console.log(items);

			break;

		} else {

			message = "User does not exist."
		}
	} 
		res.send(message);
});

app.listen(port, () => console.log(`Server is currently running at port ${port}`));