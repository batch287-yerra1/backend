// Contains instruction on HOW your API will perform its intended tasks
// All of the operations it can be done will be placed in this file

const Task = require("../models/task");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result;
	});
};

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task,error) => {


		// If an error is encountered returns a false boolean back to the p
		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};

// Controller deleting task

// The task id retrived from the "req.paramas.id" property coming from the client is renamed 
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return `${taskId} is now already deleted.`
		}
	});
};

module.exports.updateTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){

				console.log(saveErr);
				return false;
			} else {
				return "Task was added"
			}
		});
	});
};


// Getting a specific task

module.exports.gettingSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
};

// Changing the status of a task to complete

module.exports.changingStatus = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.status = "complete";

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){

				console.log(saveErr);
				return false;
			} else {
				return "Status changed to complete."
			};
		});
	});
};