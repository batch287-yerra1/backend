let http = require("http");

http.createServer(function (request, response){
	if(request.url == "/items" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrived from the datebase');
	}
	if(request.url == "/items" && request.method == 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the datebase');
	}
}).listen(4000);

console.log('Server is running at localhost:4000');