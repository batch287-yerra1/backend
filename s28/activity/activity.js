// Insert with insertOne
db.users.insertOne({
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// Insert with insertMany
db.users.insertMany([
	{
	name: "Double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "Queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple gateway",
	rooms_available: 15,
	isAvailable: false
    }
]);

// Find
db.users.find({name: "Double"});

// UpdateOne

db.users.updateOne(
	{name: "Queen"},
	{
		$set:{
			rooms_available:0
		}
	}
);

// deleteMany
db.users.deleteMany({ rooms_available: 0});