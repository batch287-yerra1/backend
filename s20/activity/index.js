// console.log("Hello World")

	let number = Number(prompt('Provide a Number'));
	console.log("The number provided is " + number);

	for( let i = number; i > 0 ; i--){
		if (i <= 50){
			console.log("The current value is <= 50, Terminating the loop.")
			break;
		} else if (i%10 === 0){
			console.log("The number is divisible by 10. Skipping the number.");
		} else if (i%5 === 0){
			console.log("The number is " + i);
		};
	};


	let givenString = "supercalifragilisticexpialidocious";
	let consonantOfString = "";

	for (let j = 0 ; j < givenString.length ; j++){
		if( 
			givenString[j] === 'a' || 
			givenString[j] === 'e' || 
			givenString[j] === 'i' || 
			givenString[j] === 'o' || 
			givenString[j] === 'u' 		
		  ){
			continue;
		} else {
			consonantOfString = consonantOfString + givenString[j];
		};
		
	}

	console.log(givenString);
	console.log(consonantOfString);	