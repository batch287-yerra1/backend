let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
}

console.log(trainer);

console.log("Result of dot notaion:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

console.log("Result of talk method");
trainer.talk();

// Creating objects out of a Pokemon constructor

function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);
			target.health = target.health - this.attack;
			console.log( target.name + "'s health is now reduced to " + target.health );
			if(target.health <= 0){
				target.faint();
			}
			
		};
		this.faint = function(){
			console.log(this.name + ' fainted ');
		};
	};

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let geodude = new Pokemon('Geodude', 8);
	console.log(geodude);
	let mewtwo = new Pokemon('Mewtwo', 100);
	console.log(mewtwo);

	geodude.tackle(pikachu);

	console.log(pikachu);

	mewtwo.tackle(geodude);
	
	console.log(geodude);
